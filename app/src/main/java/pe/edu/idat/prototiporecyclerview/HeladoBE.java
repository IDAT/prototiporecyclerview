package pe.edu.idat.prototiporecyclerview;

public class HeladoBE {
    String nombre,abreviatura;
    int foto;

    public HeladoBE() {
    }

    public HeladoBE(String nombre, String abreviatura, int foto) {
        this.nombre = nombre;
        this.abreviatura = abreviatura;
        this.foto = foto;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getAbreviatura() {
        return abreviatura;
    }

    public void setAbreviatura(String abreviatura) {
        this.abreviatura = abreviatura;
    }

    public int getFoto() {
        return foto;
    }

    public void setFoto(int foto) {
        this.foto = foto;
    }
}
