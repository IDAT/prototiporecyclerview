package pe.edu.idat.prototiporecyclerview;


import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

public class HeladoAD
        extends RecyclerView.Adapter<HeladoAD.HeladoPH> {
    private List<HeladoBE> datos;

    //CONTRUCTOR DEL ADAPTER
    HeladoAD(List<HeladoBE> datos){
        this.datos =datos;
    }

    @Override
    public HeladoPH onCreateViewHolder(ViewGroup parent, int viewType) {

        View v = LayoutInflater.
                from(parent.getContext())
                .inflate(R.layout.itemmolde,parent,false);
        HeladoPH ph = new HeladoPH(v);
        return ph;
    }

    @Override
    public void onBindViewHolder(HeladoPH holder, int position) {
        holder.im.setImageResource(datos.get(position).getFoto());
        holder.ti.setText(datos.get(position).getAbreviatura());
        holder.su.setText(datos.get(position).getNombre());
    }

    @Override
    public int getItemCount() {
        return datos.size();
    }

    public static class HeladoPH extends RecyclerView.ViewHolder{
        CardView cv;
        ImageView im;
        TextView ti;
        TextView su;
        HeladoPH(View itemView){
            super(itemView);
            cv = (CardView) itemView.findViewById(R.id.cv);
            im = (ImageView) itemView.findViewById(R.id.imgfoto);
            ti=(TextView)itemView.findViewById(R.id.txtTitulo);
            su=(TextView)itemView.findViewById(R.id.txtSubTitulo);
        }

    }
}
